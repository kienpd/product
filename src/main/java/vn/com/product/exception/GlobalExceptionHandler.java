package vn.com.product.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(Exception exception) throws Exception {
        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", "Exception: " +  exception);
        mav.addObject("timestamp", "Timestamp: " + LocalDateTime.now());
        mav.addObject("status", "HTTP Status - 500");
        mav.addObject("message", "Message: " + exception.getMessage());
        mav.setViewName("error-page");
        return mav;
    }

}
