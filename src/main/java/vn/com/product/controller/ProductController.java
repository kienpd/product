package vn.com.product.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import vn.com.product.entity.Product;
import vn.com.product.service.ProductService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
@Controller
@RequiredArgsConstructor
public class ProductController {

    private final ProductService service;
    private final MessageSource messageSource;
    private static final int[] PAGE_SIZES = {5, 10, 15, 20, 25};
    private boolean isNew;

    @RequestMapping(value = {"/", "/welcome"}, method = RequestMethod.GET)
    public String listProduct(Model model,
                              @RequestParam(value = "page", required = false, defaultValue = "1") Optional<Integer> page,
                              @RequestParam(value = "size", required = false, defaultValue = "5") Optional<Integer> size) {
        model.addAttribute("title", "Welcome");
        paging(model, page.get(), size.get());
        return "list-product";
    }

    private Page<Product> paging(Model model, int currentPage, int pageSize) {
        Page<Product> products = service.getProductsPagination(PageRequest.of(currentPage - 1, pageSize));
        model.addAttribute("products", products);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("pageSizes", PAGE_SIZES);
        model.addAttribute("selectedPage", pageSize);

        int totalPages = products.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        return products;
    }

    @RequestMapping(value = "/saveProduct", method = RequestMethod.POST)
    public String saveProduct(Model model, Product product) {
        // add product
        try {
            service.saveProduct(product);
        } catch (Exception e) {
            model.addAttribute("product", product);
            model.addAttribute("error",true);
            model.addAttribute("isNew", isNew);
            return "save-product";
        }
        model.addAttribute("products", paging(model, 1, 5));
        return "list-product";
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
    public String updateProduct(@PathVariable(value = "id") Integer id, Model model) {
        isNew = false;
        model.addAttribute("isNew", isNew);
        // find product
        model.addAttribute("title", messageSource.getMessage("label.update.product", null, LocaleContextHolder.getLocale()));
        Product product = service.getProductById(id);
        model.addAttribute("product", product);
        return "save-product";
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String createProduct(Product product, Model model) {
        model.addAttribute("title", messageSource.getMessage("label.add.product", null, LocaleContextHolder.getLocale()));
        isNew = true;
        model.addAttribute("isNew", isNew);
        return "save-product";
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String deleteProduct(@PathVariable(value = "id") Integer id, Model model) {
        // find product
        service.deleteProduct(id);
        model.addAttribute("products", paging(model, 1, 5));
        return "list-product";
    }

}
