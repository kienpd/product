package vn.com.product.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import vn.com.product.entity.Product;
import vn.com.product.repository.ProductRepository;
import vn.com.product.service.ProductService;

@Slf4j
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository repository;

    @Override
    public Page<Product> getProductsPagination(Pageable pageable) {
        return repository.findAll(pageable);
    }

    @Override
    public Product getProductById(Integer id) {
        return repository.findById(id).orElseThrow(() ->
                new RuntimeException("Product not found with id : " + id));
    }

    @Override
    public void deleteProduct(Integer id) {
        repository.deleteById(id);
    }

    @Override
    public Product saveProduct(Product product) {
        return repository.save(product);
    }
}
