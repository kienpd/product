package vn.com.product.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.product.entity.Product;

public interface ProductService {

	Page<Product> getProductsPagination(Pageable pageable);

	Product getProductById(Integer id);

	void deleteProduct(Integer id);

    Product saveProduct(Product product);
}
