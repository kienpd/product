package vn.com.product.controller;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.ui.Model;
import vn.com.product.entity.Product;
import vn.com.product.service.ProductService;

@ExtendWith(MockitoExtension.class)
public class ProductControllerTest {

  @InjectMocks
  private ProductController productController;
  @Mock
  private ProductService service;
  @Mock
  private MessageSource messageSource;

  private static final String PRODUCT_NAME = "prdName";
  private static final String PRODUCT_CODE = "prdCode";

  @Test
  public void testListProduct() {
    Model modelMock = Mockito.mock(Model.class);
    PageImpl<Product> productPage = new PageImpl<>(Collections.singletonList(buildProduct()));
    when(service.getProductsPagination(any(PageRequest.class))).thenReturn(productPage);
    String result = productController.listProduct(modelMock, Optional.of(1), Optional.of(5));
    assertTrue("list-product".equals(result));
    verify(service, times(1)).getProductsPagination(any(PageRequest.class));
  }

  @Test
  public void testSaveProduct() {
    Product product = buildProduct();
    Model modelMock = Mockito.mock(Model.class);
    when(service.saveProduct(any(Product.class))).thenReturn(product);
    when(service.getProductsPagination(any(PageRequest.class))).thenReturn(
        Mockito.mock(Page.class));
    String result = productController.saveProduct(modelMock, product);
    assertTrue("list-product".equals(result));
    verify(service, times(1)).saveProduct(any(Product.class));
    verify(service, times(1)).getProductsPagination(any(PageRequest.class));
  }

  @Test
  public void testSaveProduct_havingException() {
    Product product = buildProduct();
    Model modelMock = Mockito.mock(Model.class);
    when(service.saveProduct(any(Product.class))).thenThrow(
        new RuntimeException("having exception"));
    String result = productController.saveProduct(modelMock, product);
    assertTrue("save-product".equals(result));
    verify(service, times(1)).saveProduct(any(Product.class));
  }

  @Test
  public void testUpdateProduct() {
    Product product = buildProduct();
    Model modelMock = Mockito.mock(Model.class);
    when(service.getProductById(anyInt())).thenReturn(product);
    when(messageSource.getMessage(anyString(), any(), any())).thenReturn("message");
    String result = productController.updateProduct(1, modelMock);
    assertTrue("save-product".equals(result));
    verify(service, times(1)).getProductById(anyInt());
    verify(messageSource, times(1)).getMessage(anyString(), any(), any());
  }

  @Test
  public void testCreateProduct() {
    Product product = buildProduct();
    Model modelMock = Mockito.mock(Model.class);
    when(messageSource.getMessage(anyString(), any(), any())).thenReturn("message");
    String result = productController.createProduct(product, modelMock);
    assertTrue("save-product".equals(result));
    verify(messageSource, times(1)).getMessage(anyString(), any(), any());
  }

  @Test
  public void testDeleteProduct() {
    Product product = buildProduct();
    Model modelMock = Mockito.mock(Model.class);
    doNothing().when(service).deleteProduct(anyInt());
    when(service.getProductsPagination(any(PageRequest.class))).thenReturn(
        Mockito.mock(Page.class));
    String result = productController.deleteProduct(1, modelMock);
    assertTrue("list-product".equals(result));
    verify(service, times(1)).deleteProduct(anyInt());
    verify(service, times(1)).getProductsPagination(any(PageRequest.class));
  }


  private Product buildProduct() {
    return Product.builder()
        .id(1)
        .name(PRODUCT_NAME)
        .code(PRODUCT_CODE)
        .build();
  }

}
