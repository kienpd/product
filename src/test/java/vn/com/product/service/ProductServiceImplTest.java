package vn.com.product.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import vn.com.product.entity.Product;
import vn.com.product.repository.ProductRepository;
import vn.com.product.service.impl.ProductServiceImpl;

@ExtendWith(MockitoExtension.class)
public class ProductServiceImplTest {


  @InjectMocks
  private ProductServiceImpl productService;
  @Mock
  private ProductRepository repository;

  private static final String PRODUCT_NAME = "prdName";
  private static final String PRODUCT_CODE = "prdCode";

  @Test
  public void testGetProductsPagination() {
    PageRequest pageRequest = PageRequest.of(0, 10);
    List<Product> productList = Collections.singletonList(buildProduct());
    PageImpl productPage = new PageImpl(productList);
    when(repository.findAll(any(Pageable.class))).thenReturn(productPage);
    Page<Product> result = productService.getProductsPagination(pageRequest);
    assertNotNull(result);
    assertTrue(result.getContent().size() == 1);
  }

  @Test
  public void testGetProductById() {
    when(repository.findById(anyInt())).thenReturn(Optional.of(buildProduct()));
    Product result = productService.getProductById(1);
    assertNotNull(result);
    verify(repository, times(1)).findById(anyInt());
  }

  @Test
  public void testGetProductById_noProductFound() {
    when(repository.findById(anyInt())).thenReturn(Optional.empty());
    assertThrows(RuntimeException.class, () -> productService.getProductById(1));
    verify(repository, times(1)).findById(anyInt());
  }

  @Test
  public void testSaveProduct() {
    when(repository.save(any(Product.class))).thenReturn(buildProduct());
    Product result = productService.saveProduct(buildProduct());
    assertNotNull(result);
    verify(repository, times(1)).save(any(Product.class));
  }

  @Test
  public void testDeleteProduct() {
    doNothing().when(repository).deleteById(anyInt());
    productService.deleteProduct(1);
    verify(repository, times(1)).deleteById(anyInt());
  }

  private Product buildProduct() {
    return Product.builder()
        .id(1)
        .name(PRODUCT_NAME)
        .code(PRODUCT_CODE)
        .build();
  }

}
