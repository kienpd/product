# Example-spring-app
A starter application in spring boot


## Feature:
- Product Management with CRUD


## Requirements

For building and running the application you need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)

## Configure MariaDB

1. Create a database in your MariaDB instance.
2. Update the application.properties file in the `src/main/resources` folder with the URL, username and password for your MariaDB instance. The table schema for the Todo objects will be created for you in the database.
```properties
spring.datasource.url=jdbc:mariadb://${MARIADB_HOST}:${MARIADB_PORT}/product_schema
spring.datasource.username=${USER}
spring.datasource.password=${PASSWORD}
```
3. Prepare test data
```
CREATE DATABASE IF NOT EXISTS `product_schema` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;
USE `product_schema`;

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(9) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(90) COLLATE utf8mb4_bin NOT NULL,
  `category` varchar(28) COLLATE utf8mb4_bin NOT NULL,
  `brand` varchar(28) COLLATE utf8mb4_bin DEFAULT NULL,
  `type` varchar(21) COLLATE utf8mb4_bin DEFAULT NULL,
  `description` varchar(180) COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `UX_product_code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

INSERT INTO `products` (`id`, `code`, `name`, `category`, `brand`, `type`, `description`, `created_at`, `updated_at`) VALUES
	(1, 'P001', 'MASK ADULT Surgical 3 ply 50\'S MEDICOS with box', 'Health Accessories', 'Medicos', 'Hygiene', 'Colour: Blue (ear loop outside, ear loop inside- random assigned), Green, Purple, White, Lime Green, Yellow, Pink', '2021-11-15 14:55:30', '2021-11-15 14:55:30'),
	(2, 'P002', 'Party Cosplay Player Unknown Battlegrounds Clothes Hallowmas PUBG', 'Men\'s Clothing', 'No Brand', NULL, 'Suitable for adults and children.', '2021-11-15 14:55:30', '2021-11-15 14:55:30'),
	(3, 'P003', 'Xiaomi REDMI 8A Official Global Version 5000 mAh battery champion 31 days 2GB+32GB', 'Mobile & Gadgets', 'Xiaomi', 'Mobile Phones', 'Xiaomi Redmi 8A', '2021-11-15 14:55:30', '2021-11-15 14:55:30'),
	(4, 'P004', 'Naelofar Sofis - Printed Square', 'Hijab', 'Naelofar', 'Multi Colour Floral', 'Ornate Iris flower composition with intricate growing foliage', '2021-11-15 14:55:30', '2021-11-15 14:55:30'),
	(5, 'P005', 'Philips HR2051 / HR2056 / HR2059 Ice Crushing Blender Jar Mill', 'Small Kitchen Appliances', 'Philips', 'Mixers & Blenders', 'Philips HR2051 Blender (350W, 1.25L Plastic Jar, 4 stars stainless steel blade)', '2021-11-15 14:55:30', '2021-11-15 14:55:30'),
	(6, 'P006', 'Gemei GM-6005 Rechargeable Trimmer Hair Cutter Machine', 'Hair Styling Tools', 'Gemei', 'Trimmer', 'The GEMEI hair clipper is intended for professional use.', '2021-11-15 14:55:30', '2021-11-15 14:55:30'),
	(7, 'P007', 'Oreo Crumb Small Crushed Cookie Pieces 454g', 'Snacks', 'Oreo', 'Biscuits & Cookies', 'Oreo Crumb Small Crushed Cookie Pieces 454g - Retail & Wholesale New Stock Long Expiry!!!', '2021-11-15 14:55:31', '2021-11-15 14:55:31'),
	(8, 'P008', 'Non-contact Infrared Forehead Thermometer ABS', 'Kids Health & Skincare', 'No Brand', NULL, 'Non-contact Infrared Forehead Thermometer ABS for Adults and Children with LCD Display Digital', '2021-11-15 14:55:31', '2021-11-15 14:55:31'),
	(9, 'P009', 'Nordic Marble Starry Sky Bedding Sets', 'Bedding', 'No Brand', 'Bedding Sheets', 'Printing process: reactive printing. Package：quilt cover ,bed sheet ,pillow case. Not include comforter or quilt.', '2021-11-15 14:55:31', '2021-11-15 14:55:31'),
	(10, 'P010', 'Samsung Galaxy Tab A 10.1"', 'Mobile & Gadgets', 'Samsung', 'Tablets', '4GB RAM. - 64GB ROM. - 1.5 ghz Processor. - 10.1 inches LCD Display', '2021-11-15 14:55:31', '2021-11-15 14:55:31'),
	(11, 'P011', 'REALME 5 PRO 6+128GB', 'Mobile & Gadgets', 'Realme', 'Mobile Phones', 'REALME 5 PRO 6+128GB', '2021-11-15 14:55:31', '2021-11-15 14:55:31'),
	(12, 'P012', 'Nokia 2.3 - Cyan Green', 'Mobile & Gadgets', 'Nokia', 'Mobile Phones', 'Nokia smartphones get 2 years of software upgrades and 3 years of monthly security updates.', '2021-11-15 14:55:31', '2021-11-15 14:55:31'),
	(13, 'P013', 'AKEMI Cotton Select Fitted Bedsheet Set - Adore 730TC', 'Bedding', 'Akemi', 'Bedding Sheets', '100% Cotton Twill. Super Single.', '2021-11-15 14:55:31', '2021-11-15 14:55:31'),
	(14, 'P014', 'Samsung Note10+ Phone', 'Mobile & Gadgets', 'OEM', 'Mobile Phones', 'OEM Phone Models', '2021-11-15 14:55:31', '2021-11-15 14:55:31'),
	(15, 'P015', 'Keknis Basic Wide Long Shawl', 'Hijab', 'No Brand', 'Plain Shawl', '1.8m X 0.7m (+/-). Heavy chiffon (120 gsm).', '2021-11-15 14:55:31', '2021-11-15 14:55:31');
```

## Running the application locally
Need to build first

```shell
mvn clean install
```

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `vn.com.product.ProductApplication` class from your IDE.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
mvn spring-boot:run
```
